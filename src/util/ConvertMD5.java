package util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ConvertMD5 {

	public static String md5(String value) {

		String md5Value = "";
		MessageDigest m;

		try {
			
			m = MessageDigest.getInstance("MD5");
			m.update(value.getBytes(),0,value.length());
			md5Value = new BigInteger(1, m.digest()).toString(16);

			return md5Value;

		} catch (NoSuchAlgorithmException e) {

			return null;

		}

	}
	
}