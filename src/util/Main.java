package util;

import business.Fachada;
import model.Telefone;
import model.Usuario;

public class Main {

	public static void main(String[] args) throws Exception {

		Usuario u = new Usuario();
		Telefone t = new Telefone();
		Fachada fachada = new Fachada();

		u.setNome("Santana");
		u.setSenha("Santana");
		u.setEmail("santana@teste.com");

		t.setDdd(111);
		t.setNumero("356854125");
		t.setTipo("r");

		u.setTelefone(t);

//		fachada.incluirTelefones(t);
		fachada.incluirUsuario(u);

	}

}
