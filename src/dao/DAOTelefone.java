package dao;

import model.Telefone;

public class DAOTelefone extends DAOGeneric<Telefone> {

	public DAOTelefone() {
		super(Telefone.class);
	}

}
