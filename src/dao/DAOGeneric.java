package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import util.JPAUtil;

public abstract class DAOGeneric<T> {

	private Class<T> persistedClass;
	EntityManager entityManager = JPAUtil.getEntityManager();
	EntityTransaction entityTransation = entityManager.getTransaction();

	protected DAOGeneric() {
	}

	protected DAOGeneric(Class<T> persistedClass) {
		this.persistedClass = persistedClass;
	}

	public void incluir(T t) {

		try {

			entityManager.getTransaction().begin();
			entityManager.persist(t);
			entityManager.getTransaction().commit();
			entityManager.close();

		} catch (PersistenceException e) {

			System.out.println(e.getMessage());
			entityTransation.rollback();

		}

	}

	public void alterar(T t) {

		try {

			entityManager.getTransaction().begin();
			entityManager.merge(t);
			entityManager.getTransaction().commit();
			entityManager.close();

		} catch (PersistenceException e) {

			System.out.println(e.getMessage());
			entityTransation.rollback();

		}

	}

	public void excluir(T t) {

		try {

			entityManager.getTransaction().begin();
			entityManager.remove(entityManager.merge(t));
			entityManager.getTransaction().commit();
			entityManager.close();

		} catch (PersistenceException e) {

			System.out.println(e.getMessage());
			entityTransation.rollback();

		}

	}

	@SuppressWarnings("unchecked")
	public List<T> listar() {

		try {

			entityManager.getTransaction().begin();
			List<T> lista = entityManager.createQuery("from " + persistedClass.getSimpleName()).getResultList();
			entityManager.getTransaction().commit();
			entityManager.close();
			return lista;

		} catch (PersistenceException e) {

			System.out.println(e.getMessage());
			entityTransation.rollback();
			return null;

		}
		
	}

	public T getById(Long id) {

		try {

			entityManager.getTransaction().begin();
			T t = entityManager.find(persistedClass, id);
			entityManager.close();
			return t;

		} catch (PersistenceException e) {

			System.out.println(e.getMessage());
			entityTransation.rollback();
			return null;

		}
		
	}

}
