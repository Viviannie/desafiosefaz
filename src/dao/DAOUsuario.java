package dao;

import java.util.List;

import javax.persistence.Convert;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import model.Usuario;
import util.ConvertMD5;
import util.JPAUtil;

public class DAOUsuario extends DAOGeneric<Usuario> {

	EntityManager entityManager = JPAUtil.getEntityManager();
	EntityTransaction entityTransation = entityManager.getTransaction();

	public DAOUsuario() {
		super(Usuario.class);
	}

	public boolean efetuarLogin(Usuario usuario) throws Exception {

		boolean result;
		List<Usuario> lista = null;

		try {

			String q = " SELECT u FROM Usuario u WHERE email = :email AND senha = :senha ";
			TypedQuery<Usuario> query = entityManager.createQuery(q, Usuario.class);
			query.setParameter("email", usuario.getEmail());
			query.setParameter("senha", usuario.getSenha()); //ConvertMD5.md5(usuario.getEmail() + usuario.getSenha())

			lista = query.getResultList();

			if (lista.size() > 0) {

				result = true;

			} else {

				result = false;

			}

			return result;

		} catch (Exception ex) {

			throw new Exception("Erro ao efetuar login!");
		}

	}

	public Usuario getUsuarioByEmail(String email) {

		Usuario usuario = new Usuario();

		try {

			String q = " SELECT u FROM Usuario u WHERE email = :email";
			TypedQuery<Usuario> query = entityManager.createQuery(q, Usuario.class);
			query.setParameter("email", email);

			usuario = query.getSingleResult();

			return usuario;

		} catch (PersistenceException e) {

			System.out.println(e.getMessage());
			entityTransation.rollback();
			return null;

		}

	}

}
