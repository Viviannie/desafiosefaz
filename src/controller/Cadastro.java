package controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import business.Fachada;
import model.Telefone;
import model.Usuario;

@Named
@RequestScoped
public class Cadastro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioLogado usuarioLogado;

	private List<Usuario> usuarios;

	private Usuario usuario = new Usuario();
	
	private Usuario usuarioSelecionado = new Usuario();

	Fachada fachada = new Fachada();

	private List<Telefone> telefones;
	private Telefone telefone = new Telefone();

	public String inserir() {

		try {

			usuario.setTelefone(this.telefone);
			fachada.incluirUsuario(this.usuario);
			addMessage("Usu�rio inclu�do com sucesso!");

			return "home";

		} catch (Exception e) {

			e.printStackTrace();
			return null;

		}

	}

	public String paginaEditar() throws Exception {

		if (fachada.getUsuarioById(this.usuario.getId()) != null) {

//			usuario = fachada.getUsuarioById(usuario.getId());

			return "editar_usuario";

		} else {

			return null;

		}

	}

	public String editar() throws Exception {

		if (this.usuario != null) {

			fachada.alterarUsuario(usuario);

			return "listar_usuarios";

		} else {

			return null;

		}

	}

	public String excluir() throws IOException {

		fachada.excluirUsuario(this.usuario);
		return "listar_usuarios";

	}

	public List<Usuario> listarUsuarios() {

		if (usuarios == null) {

			usuarios = fachada.getUsuarios();

			return usuarios;

		} else {

			return null;

		}

	}

	public void setTelefones(List<Telefone> telefones) {

		if (getTelefones() == null) {

			this.telefones = telefones;

		}

	}

	public Telefone getTelefone() {

		if (telefone == null) {

			setTelefone(new Telefone());

		}

		return telefone;

	}

	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}

	public String cancelar() {
		return "home";
	}

	public void addMessage(String summary) {

		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);

	}

	public UsuarioLogado getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(UsuarioLogado usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;
	}

	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}
	
}
