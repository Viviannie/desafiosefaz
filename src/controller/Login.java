package controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import business.Fachada;
import model.Usuario;

@Named
@RequestScoped
public class Login implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
    private UsuarioLogado usuarioLogado;

	private List<Usuario> usuarios;
	
	private Usuario usuario = new Usuario();
	private String email;
	private String senha;

	Fachada fachada = new Fachada();
	
	public String logar() throws Exception {

		if (fachada.efetuarLogin(this.usuario)) {
			
			usuario = fachada.getUsuarioByEmail(usuario.getEmail());
			this.usuarioLogado.setUsuario(usuario);			
			return "listar_usuarios";

		} else {

			FacesContext ctx = FacesContext.getCurrentInstance();
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usu�rio inv�lido!", "Usu�rio inv�lido!");
			ctx.addMessage(null, msg);
			return null;

		}

	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String logOff() {

		FacesContext fc = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
		session.invalidate();
		return "home";

	}

	public boolean isUsuarioLogado() {
		return usuario != null;
	}

	public String cadastrar() {
		return "cadastro_usuario";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public UsuarioLogado getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(UsuarioLogado usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}
	
}
