package business;

import java.util.List;

import dao.DAOTelefone;
import model.Telefone;

public class BusinessTelefone {

	private final DAOTelefone daoTelefones = new DAOTelefone();

	public void incluir(Telefone t) {

		daoTelefones.incluir(t);
	}

	public void alterar(Telefone t) {

		daoTelefones.alterar(t);

	}

	public void excluir(Telefone t) {

		daoTelefones.excluir(t);

	}
	
	public Telefone getTelefonesById(Long id) {

		return daoTelefones.getById(id);

	}

	public List<Telefone> getTelefones() {

		return daoTelefones.listar();

	}

	public void validarDDD(Telefone t) throws Exception {

		if (t.getNumero().trim().length() != 3) {

			throw new Exception("DDD inv�lido!");

		}
	}

	public void validarNumero(Telefone t) throws Exception {

		if ((t.getNumero().trim().length() <= 6) && (t.getNumero().trim().length() >= 50)) {

			throw new Exception("N�mero inv�lido!");

		}
	}

}
