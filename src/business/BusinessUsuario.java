package business;

import java.util.List;

import dao.DAOUsuario;
import model.Usuario;

public class BusinessUsuario {

	private final DAOUsuario daoUsuario = new DAOUsuario();	

	public void incluir(Usuario u) {

		daoUsuario.incluir(u);
		
	}
	
	public void alterar(Usuario u) {

		daoUsuario.alterar(u);

	}	

	public void excluir(Usuario u) {

		daoUsuario.excluir(u);         

	}

	public Usuario getUsuarioById(Long id) {

		return daoUsuario.getById(id);

	}
	
	public boolean efetuarLogin(Usuario usuario) throws Exception {
		
		return daoUsuario.efetuarLogin(usuario);
		
	}
	
	public Usuario getUsuarioByEmail(String email) {
		
		return daoUsuario.getUsuarioByEmail(email);
		
	}

	public List<Usuario> getUsuarios() {

		return daoUsuario.listar();

	}	

	public void validarNome(Usuario u) throws Exception {

		if ((u.getNome() == null) || (u.getNome().trim().equals("")) || (u.getNome().trim().length() <= 6 && u.getNome().trim().length() >= 50)) {

			throw new Exception("Nome do usu�rio inv�lido!");

		}
	}
	
	public void validarEmail(Usuario u) throws Exception  {

		if ((u.getEmail() == null) || (u.getEmail().trim().equals("")) || (u.getEmail().trim().length() <= 10 && u.getEmail().trim().length() >= 80)) {

			throw new Exception("E-mail do usu�rio inv�lido!");

		}
	}
	
	public void validarSenha(Usuario u) throws Exception  {

		if ((u.getSenha() == null) || (u.getSenha().trim().equals(""))) {

			throw new Exception("Senha do usu�rio inv�lida!");

		}
	}
	
	public void validarTelefone(Usuario u) throws Exception {

		if ((u.getTelefone() == null)) {

			throw new Exception("Informe o telefone da usu�rio!");

		}
	}

}
