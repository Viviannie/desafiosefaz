package business;

import java.util.List;

import model.Telefone;
import model.Usuario;

public class Fachada {
	
	private static Fachada instancia;

	private static BusinessUsuario rnUsuario;
	private static BusinessTelefone rnTelefones;

	public Fachada() {

		rnUsuario = new BusinessUsuario();
		rnTelefones = new BusinessTelefone();

	}

	public static Fachada getInstancia() {

		if (instancia == null) {

			instancia = new Fachada();

		}

		return instancia;

	}

	//Usu�rio
	public void incluirUsuario(Usuario usuario) throws Exception {

		rnUsuario.validarEmail(usuario);
		rnUsuario.validarNome(usuario);
		rnUsuario.validarSenha(usuario);
		rnUsuario.validarTelefone(usuario);
		rnUsuario.incluir(usuario);

	}

	public void alterarUsuario(Usuario usuario) throws Exception {

		rnUsuario.validarEmail(usuario);
		rnUsuario.validarNome(usuario);
		rnUsuario.validarSenha(usuario);
		rnUsuario.validarTelefone(usuario);
		rnUsuario.alterar(usuario);

	}

	public void excluirUsuario(Usuario usuario) {

		rnUsuario.excluir(usuario);

	}
	
	public Usuario getUsuarioById(Long id) {

		return rnUsuario.getUsuarioById(id);

	}
	
	public Usuario getUsuarioByEmail(String email) {
		
		return rnUsuario.getUsuarioByEmail(email);
		
	}
	
	public boolean efetuarLogin(Usuario usuario) throws Exception {
		
		return rnUsuario.efetuarLogin(usuario);
		
	}

	public List<Usuario> getUsuarios(){

		return rnUsuario.getUsuarios();

	}

	//Telefone
	public void incluirTelefones(Telefone telefone) throws Exception {

		rnTelefones.validarDDD(telefone);
		rnTelefones.validarNumero(telefone);
		rnTelefones.incluir(telefone);

	}

	public void alterarTelefones(Telefone telefone) throws Exception {

		rnTelefones.validarDDD(telefone);
		rnTelefones.validarNumero(telefone);
		rnTelefones.alterar(telefone);

	}

	public void excluirTelefones(Telefone cliente) throws Exception {

		rnTelefones.excluir(cliente);

	}
	
	public Telefone getTelefonesById(Long id) {

		return rnTelefones.getTelefonesById(id);

	}

	public List<Telefone> getTelefones(){

		return rnTelefones.getTelefones();

	}

}
