package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;

@Embeddable
@Table
public class Telefone implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length=3, nullable=true)
	private Integer ddd;
	@Column(length=9, nullable=true)
	private String numero;
	@Column(length=7, nullable=true)
	private String tipo;
	
	public Integer getDdd() {
		return ddd;
	}
	
	public void setDdd(Integer ddd) {
		this.ddd = ddd;
	}
	
	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
